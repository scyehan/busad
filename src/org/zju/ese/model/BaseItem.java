package org.zju.ese.model;

import java.util.Date;

public class BaseItem {
	Date updateTime;
	String fileName;
	String name;
	String path;
	boolean needDownload;
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isNeedDownload() {
		return needDownload;
	}
	public void setNeedDownload(boolean needDownload) {
		this.needDownload = needDownload;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
}
