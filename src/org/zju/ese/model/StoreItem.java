package org.zju.ese.model;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StoreItem {
	Map<String,AdPoint> adPointMap = new HashMap<String,AdPoint>();
	Map<String,Film> filmMap = new HashMap<String,Film>();
	
	public Map<String, AdPoint> getAdPointMap() {
		return adPointMap;
	}

	public void setAdPointMap(Map<String, AdPoint> adPointMap) {
		this.adPointMap = adPointMap;
	}

	public Map<String, Film> getFilmMap() {
		return filmMap;
	}

	public void setFilmMap(Map<String, Film> filmMap) {
		this.filmMap = filmMap;
	}
	
	private void updateMap(Map oldMap,Map newMap,String basePath)
	{
		Set<String> nameSet = new HashSet<String>();
		for(Object key : newMap.keySet())
		{
			BaseItem newItem = (BaseItem)newMap.get(key);
			BaseItem oldItem = (BaseItem)oldMap.get(key);
			nameSet.add(key.toString());
			if(oldItem != null)
			{
				if(newItem.getUpdateTime().after(oldItem.getUpdateTime()))
				{
					File f = new File(basePath +"/"+ oldItem.getPath());  
					f.delete();
					oldMap.put(key, newItem);
					newItem.needDownload = true;
				}
				
			}
			else
			{
				oldMap.put(key, newItem);
				newItem.needDownload = true;
			}
		}
		
		for(Object key : oldMap.keySet())
		{
			if(!nameSet.contains(key))
			{
				BaseItem item = (BaseItem)oldMap.get(key);
				File f = new File(basePath + item.getPath());  
				f.delete();
			}
		}
	}
	
	public void updateAdMap(Map<String,AdPoint> newMap,String basePath)
	{
		if(newMap != null)
		{
			updateMap(adPointMap,newMap,basePath);
		}
	}
	
	public void updateFilmMap(Map<String,Film> newMap,String basePath)
	{
		if(newMap != null)
		{
			updateMap(filmMap,newMap,basePath);
		}
	}
}
