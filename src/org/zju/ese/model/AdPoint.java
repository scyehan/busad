package org.zju.ese.model;

import java.util.Date;

public class AdPoint extends BaseItem {
	public static final String PathPrefix = "video/";
	double latitude;
	double longitude;
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public String getPath()
	{
		return PathPrefix + fileName;
	}
}
