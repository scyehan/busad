package org.zju.ese.bus;

import org.zju.ese.bus.R;

import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.Preference.OnPreferenceChangeListener;
import android.view.KeyEvent;
import android.widget.Toast;

public class SettingActivity extends PreferenceActivity implements OnPreferenceChangeListener {
	EditTextPreference addressPreference;
	EditTextPreference portPreference;
	EditTextPreference idPreference;
	
	String address;
	String port;
	String id;
	
	@Override
	public void onCreate(Bundle icicle) {		
		super.onCreate(icicle);
		addPreferencesFromResource(R.layout.preference);
		addressPreference = (EditTextPreference)findPreference("pref_server_address");
		portPreference = (EditTextPreference)findPreference("pref_server_port");
		idPreference = (EditTextPreference)findPreference("pref_my_id");
		
		addressPreference.setOnPreferenceChangeListener(this);
		portPreference.setOnPreferenceChangeListener(this);
		idPreference.setOnPreferenceChangeListener(this);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		// TODO Auto-generated method stub
		if(addressPreference == preference)
			address = newValue.toString();
		else if(portPreference == preference)
			port = newValue.toString();
		else if(idPreference == preference)
			id = newValue.toString();
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if((keyCode == KeyEvent.KEYCODE_BACK)&&(event.getAction() == KeyEvent.ACTION_DOWN))  
		{  
			Intent intent = new Intent();
			 if(address != null)
				 intent.putExtra("address", address);
			 if(port != null)
				 intent.putExtra("port", port);
			 if(id != null)
				 intent.putExtra("id", id);
	         setResult(RESULT_OK, intent);
	         finish();
	         return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
