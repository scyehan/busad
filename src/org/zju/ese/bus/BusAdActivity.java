package org.zju.ese.bus;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnCompletionListener;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.TimerTask;

import org.codehaus.jackson.JsonParseException;
import org.zju.ese.bus.R;
import org.zju.ese.model.Film;
import org.zju.ese.model.StoreItem;
import org.zju.ese.util.PojoMapper;

public class BusAdActivity extends Activity implements OnCompletionListener{
	private EditText latitudeText;
	private EditText longitudeText;
	private Button button;
	private VideoView mVideoView;
	private LocationManager lm;
	private double latitudeNTU;
	private double longitudeNTU;
	private String pathA = "/sdcard/a.mp4";
	private String pathB = "/sdcard/b.mp4";
	private long position;
	private boolean playingAd = false;
	private String basePath;
	private StoreItem storeData;
	private Iterator<Film> filmIterator;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
			return;
        basePath = getExternalFilesDir(null).getAbsolutePath();
        setContentView(R.layout.activity_bus_ad);
        mVideoView = (VideoView) findViewById(R.id.surface_view);
        mVideoView.setOnCompletionListener(this);
        mVideoView.setVideoQuality(MediaPlayer.VIDEOQUALITY_HIGH);
		mVideoView.setMediaController(new MediaController(BusAdActivity.this));
		
        latitudeText = (EditText) findViewById(R.id.latitude);
        longitudeText = (EditText) findViewById(R.id.longitude);
        button = (Button)findViewById(R.id.startBtn);
        
//        Intent intent = getIntent();
//        String path = basePath + "/" + intent.getStringExtra("file");
//        mVideoView.setVideoPath(path);
        readStoreData();
        playNextFilm();
        
        button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				lm = (LocationManager)BusAdActivity.this.getSystemService(Context.LOCATION_SERVICE);
				String bestProvider = lm.getBestProvider(getCriteria(), true);	
				lm.requestLocationUpdates(bestProvider, 5000, 8, ll);	
				
				latitudeNTU = Float.parseFloat(latitudeText.getText().toString()) * 100000;
				longitudeNTU = Float.parseFloat(longitudeText.getText().toString()) * 100000;
				//latitudeNTU = 30.261760 * 100000;
				//longitudeNTU = 120.126161 * 100000;
				
				mVideoView.setVideoPath(pathA);
				
				//Timer timer = new Timer(true);
				//timer.schedule(task, 10000);
			}
        });
    }
    
    private void initServer()
    {
    	SharedPreferences mPerferences = PreferenceManager.getDefaultSharedPreferences(this);
    	
    }
    
    private void readStoreData()
    {
		try {
			FileReader fr = new FileReader(basePath+"/data.json");
			storeData = (StoreItem) PojoMapper.fromJson(fr, StoreItem.class);
			filmIterator = storeData.getFilmMap().values().iterator();
			fr.close();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void playNextFilm()
    {
    	if(filmIterator.hasNext())
    	{
    		Film film = filmIterator.next();
    		String filmPath = basePath + "/film/" + film.getFileName();
    		mVideoView.setVideoPath(filmPath);
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_bus_ad, menu);
        return true;
    }
    
    public static Criteria getCriteria(){
    	Criteria c = new Criteria();
    	c.setAccuracy(Criteria.ACCURACY_COARSE);	//设置查询精度
    	c.setSpeedRequired(false);					//设置是否要求速度
    	c.setCostAllowed(true);					//设置是否允许产生费用
    	c.setBearingRequired(false);				//设置是否需要得到方向
    	c.setAltitudeRequired(false);				//设置是否需要得到海拔高度
    	c.setPowerRequirement(Criteria.POWER_LOW);	//设置允许的电池消耗级别
    	return c;									//返回查询条件
    }
    
    boolean isNear(double latitude,double longitude)
    {
    	double laDistance = latitude * 100000 - latitudeNTU;
    	double loDistance = longitude * 100000 - longitudeNTU;
    	return Math.sqrt(Math.pow(laDistance, 2)+Math.pow(loDistance, 2)) < 10;
    }
    
    @Override
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
    	if(playingAd)
    	{
    		mVideoView.setVideoPath(pathA,position);
    		playingAd = false;
    	}
    	else
    		playNextFilm();
	}
    
    TimerTask task = new TimerTask(){  
		public void run() {  
			handler.obtainMessage().sendToTarget();
		}  
	}; 
	
	private Handler handler = new Handler()
	{
		 @Override
	     public void handleMessage(Message msg) 
		 {
			 position = mVideoView.getCurrentPosition();
			 mVideoView.setVideoPath(pathB);
		 }
	};
    
    LocationListener ll = new LocationListener(){
		public void onLocationChanged(Location location) {//重写onLocationChanged方法
			if(location != null)
			{
				if(isNear(location.getLatitude(),location.getLongitude()))
					if(!playingAd)
					{
						playingAd = true;
						position = mVideoView.getCurrentPosition();
						mVideoView.setVideoPath(pathB);
					}
			}	
		}
		public void onProviderDisabled(String provider) {	//重写onProviderDisabled方法
			
		}
		public void onProviderEnabled(String provider) {	//重写onProviderEnabled方法
	        Location l= lm.getLastKnownLocation(provider);		//获取位置信息
	        Toast.makeText(BusAdActivity.this, "enable", Toast.LENGTH_LONG).show();
		}
		public void onStatusChanged(String provider, int status, Bundle extras) {	//重写onStatusChanged方法
		}
	};
}
