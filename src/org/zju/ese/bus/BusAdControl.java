package org.zju.ese.bus;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.zju.ese.bus.R;
import org.zju.ese.model.BaseItem;
import org.zju.ese.model.Film;
import org.zju.ese.model.StoreItem;
import org.zju.ese.util.HttpHelper;
import org.zju.ese.util.PojoMapper;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class BusAdControl extends Activity {
	public static final String TAG = "BusAdControl";
	public static final int MSGSTOREDATA = 0;
	public static final int MSGDOWNLOADFINISH = 1;
	private Button button;
	private String basePath;
	DownloadManager manager ;  
    DownloadCompleteReceiver receiver;  
    HttpHelper httpHelper;
    Map<Long,BaseItem> downloadMap = new HashMap<Long,BaseItem>();
    StoreItem newStoreData;
    StoreItem storeData;
    Iterator<Film> filmIterator;
    
    String serverIp;
    String serverPort;
    String busId;
    String baseUrl;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_ad_control);
        basePath = getExternalFilesDir(null).getAbsolutePath();
        manager =(DownloadManager)getSystemService(DOWNLOAD_SERVICE);  
        receiver = new DownloadCompleteReceiver();  
        button = (Button)findViewById(R.id.startBtn);
        button.setOnClickListener(new OnClickListener() {  
            @Override  
            public void onClick(View v) {  
            	httpHelper.getForObject(MSGSTOREDATA,baseUrl + "/bus/" + busId,StoreItem.class);
            	button.setEnabled(false);
            }  
        });
        readStoreData();
        httpHelper = new HttpHelper(mHandler);
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        initServer();
    }
    
    @Override  
    protected void onResume() {  
        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));  
        super.onResume();  
    }  
      
    @Override  
    protected void onDestroy() {  
        if(receiver != null)unregisterReceiver(receiver);  
        super.onDestroy();  
    }  

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_bus_ad_control, menu);
        return true;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
    	if(item.getItemId() == R.id.menu_settings)
    	{
    		Intent intent = new Intent(this,SettingActivity.class);
    		startActivityForResult(intent,R.id.menu_settings);
    	}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if(requestCode == R.id.menu_settings)
		{
			initServer();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void initServer()
    {
    	SharedPreferences mPerferences = PreferenceManager.getDefaultSharedPreferences(this);
    	serverIp = mPerferences.getString("pref_server_address", "42.121.56.72");
    	serverPort = mPerferences.getString("pref_server_port", "8000");
    	busId = mPerferences.getString("pref_my_id", "1");
    	//Toast.makeText(this,serverIp+serverPort+busId, Toast.LENGTH_SHORT).show();
    	baseUrl = "http://" + serverIp + ":" + serverPort;
    }
	
	private void downlaodNext()
	{
    	if(filmIterator.hasNext())
    	{
    		Film film = filmIterator.next();
    		while(!film.isNeedDownload())
    			if(filmIterator.hasNext())
    				film = filmIterator.next();
    			else
    			{
    				Toast.makeText(BusAdControl.this, "下载完成", Toast.LENGTH_LONG).show();
    				startPlayFilm();
    				return;
    			}
    		download(film);
    	}
    	else
    	{
    		Toast.makeText(BusAdControl.this, "下载完成", Toast.LENGTH_LONG).show();
    		startPlayFilm();
    	}
	}
	
	private void startPlayFilm()
	{
		Intent newIntent = new Intent(BusAdControl.this,BusAdActivity.class);
        this.startActivity(newIntent);
        button.setEnabled(true);
	}
	
	private void download(BaseItem item)
	{
		//创建下载请求  
        DownloadManager.Request down=new DownloadManager.Request (Uri.parse(baseUrl + "/" + item.getPath()));  
        //设置允许使用的网络类型，这里是移动网络和wifi都可以  
        down.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE|DownloadManager.Request.NETWORK_WIFI);  
        //禁止发出通知，既后台下载  
        down.setShowRunningNotification(true);  
        //不显示下载界面  
        down.setVisibleInDownloadsUi(false);  
        //设置下载后文件存放的位置  
        down.setDestinationInExternalFilesDir(BusAdControl.this, null, item.getPath());  
        //将下载请求放入队列  
        long id = manager.enqueue(down);
        downloadMap.put(id, item);
	}
	
	private void readStoreData()
    {
		try {
			FileReader fr = new FileReader(basePath+"/data.json");
			storeData = (StoreItem) PojoMapper.fromJson(fr, StoreItem.class);
			filmIterator = storeData.getFilmMap().values().iterator();
			fr.close();
		} catch (JsonParseException e) {
			storeData = new StoreItem();
			e.printStackTrace();
		} catch (IOException e) {
			storeData = new StoreItem();
			e.printStackTrace();
		}
    }
	
	private void writeStoreData(StoreItem item)
    {
		try {
			FileWriter fw = new FileWriter(basePath+"/data.json");
			PojoMapper.toJson(item, fw, true);
			fw.close();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	private Handler mHandler = new Handler()
	{
		 @Override
	     public void handleMessage(Message msg) 
		 {
			 switch(msg.what)
			 {
			 case MSGSTOREDATA:
				 if(msg.arg1 == HttpHelper.REQUEST_SUCCESS)
				 {
					 newStoreData = (StoreItem)msg.obj;
					 storeData.updateAdMap(newStoreData.getAdPointMap(), basePath);
					 storeData.updateFilmMap(newStoreData.getFilmMap(), basePath);
					 writeStoreData(storeData);
					 filmIterator = storeData.getFilmMap().values().iterator();
					 downlaodNext();
				 }
				 else
					 Toast.makeText(BusAdControl.this, "网络读取错误，请检查网络连接和服务器设置！", Toast.LENGTH_LONG).show();
				 break;
			 case MSGDOWNLOADFINISH:
				 long downId = msg.arg1;
				 BaseItem item = downloadMap.get(downId);
				 Cursor cursor = manager.query(new DownloadManager.Query().setFilterById(downId));
				 if(cursor != null && cursor.moveToFirst() && item != null){
					 int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
					 if(status == DownloadManager.STATUS_SUCCESSFUL)
					 {
						 Toast.makeText(BusAdControl.this, item.getFileName()+" finish", Toast.LENGTH_SHORT).show(); 
						 downloadMap.remove(downId);
						 item.setNeedDownload(false);
						 writeStoreData(storeData);
						 downlaodNext();
					 }
					 else if(status == DownloadManager.STATUS_FAILED)
					 {
						 Toast.makeText(BusAdControl.this, item.getFileName()+" failue", Toast.LENGTH_SHORT).show(); 
						 download(item);
					 }
				 }
				 
				 break;
			 }
		 }
	};

	class DownloadCompleteReceiver extends BroadcastReceiver {  
        @Override  
        public void onReceive(Context context, Intent intent) {  
            if(intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)){  
                long downId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);  
                //Log.v(TAG," download complete! id : "+downId);  
                
            	Message message = mHandler.obtainMessage(MSGDOWNLOADFINISH);
            	message.arg1 = (int)downId;
            	message.sendToTarget();
                
            }  
        }  
    }  
}
