package org.zju.ese.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.os.Handler;
import android.os.Message;

public class HttpHelper {
	public static final int REQUEST_SUCCESS = 0;
	public static final int REQUEST_FAIL = 1;
	RestTemplate restTemplate = new RestTemplate();
	Handler handler;
	
	public HttpHelper(Handler handler)
	{
		this.handler = handler;
		ObjectMapper mapper = new ObjectMapper();
		
		MappingJacksonHttpMessageConverter messageConverter = new MappingJacksonHttpMessageConverter();
		messageConverter.setObjectMapper(mapper);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(messageConverter);
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));
		// Create a new RestTemplate instance
		restTemplate.setMessageConverters(messageConverters);
		// Add the Jackson message converter
		restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
	}
	
	public <T> void getForObject(final int what,final String url,final Class<T> c)
	{
		Thread thread = new Thread(){
			public void run()
	    	{
				Message message = null;
				try{
					Object result = restTemplate.getForObject(url,c);
					message = handler.obtainMessage(what, result);
					message.arg1 = REQUEST_SUCCESS;
				}catch(Exception e)
				{
					message = handler.obtainMessage(what);
					message.arg1 = REQUEST_FAIL;
				}
				finally{
					if(message != null)
						message.sendToTarget();
				}
	    	}
		};
		thread.start();
	}
}
